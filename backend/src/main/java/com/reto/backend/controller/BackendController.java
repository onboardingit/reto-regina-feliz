package com.reto.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.backend.api.BackendApi;
import com.reto.backend.exception.BackendException;
import com.reto.backend.model.Chapter;
import com.reto.backend.model.Character;
import com.reto.backend.model.Movie;
import com.reto.backend.model.Quote;
import com.reto.backend.service.ChapterService;
import com.reto.backend.service.CharacterService;
import com.reto.backend.service.MovieService;
import com.reto.backend.service.QuoteService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
public class BackendController implements BackendApi {

    @Autowired
    private MovieService movieService;
    
    @Autowired
    private CharacterService characterService;
    
    @Autowired
    private ChapterService chapterService;
    
    @Autowired
    private QuoteService quoteService;
    
    
    @PostMapping("/health-check")
    public ResponseEntity<String> healthCheck(){

        return ResponseEntity.ok("Hello Backend Reto Regina Feliz");

    }


    @PostMapping("/movie")
    public ResponseEntity<List<Movie>> movie() throws BackendException{
    	
        return ResponseEntity.ok(movieService.getMovies());

    }
    
    @PostMapping("/charater")
    public ResponseEntity<List<Character>> charater() throws BackendException{
    	
        return ResponseEntity.ok(characterService.getCharacter());

    }
    
    @PostMapping("/chapter/{bookId}/order/{sort}")
    public ResponseEntity<List<Chapter>> chapter(@PathVariable("bookId") String bookId, 
    												@PathVariable("sort") String sort) throws BackendException{

    	return ResponseEntity.ok(chapterService.getChapters(bookId, sort));

    }
    
    @PostMapping("/quote/{characterId}/order/{sort}")
    public ResponseEntity<List<Quote>> quote(@PathVariable("characterId") String characterId, 
												@PathVariable("sort") String sort) throws BackendException{

    	return ResponseEntity.ok(quoteService.getQuotes(characterId, sort));

    }
    
}
