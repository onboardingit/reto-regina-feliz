package com.reto.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.backend.exception.BackendException;
import com.reto.backend.model.Quote;

@Service
public class QuoteService {
	
	@Value("${business.service.host}")
	private String authServiceHost;	
		
    public List<Quote> getQuotes(String characterId, String sort) throws BackendException{
    	
    	List<Quote> quotes = externalRequest(characterId, sort);
    	
	    if (quotes == null) {
	    	
	        throw new BackendException("1050", "Error al obtener las frases",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	    }
	    
        return quotes;

    }
	  
	  
	private List<Quote> externalRequest(String characterId, String sort){
	  	
		String url = authServiceHost + "/quote/" + characterId + "/order/" + sort;
		
		List<Quote> quotes = null;
		try {
			
			quotes = getRequest(url);
			
		} catch (BackendException e) {
			
			e.printStackTrace();
		}
		  
		return quotes;
	
	}
  
	private List<Quote> getRequest(String url) throws BackendException {        

        RestTemplate restTemplate = new RestTemplate();
      
        ResponseEntity<List<Quote>> responseEntity =
        		restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Quote>>() {
        		        });   
        
	    if (responseEntity == null) {
	    	
	        throw new BackendException("1050", "Error de Conexion al Business",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return responseEntity.getBody();
    }

}
