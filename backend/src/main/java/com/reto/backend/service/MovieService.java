package com.reto.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.backend.exception.BackendException;
import com.reto.backend.model.Movie;


@Service
public class MovieService {
	
	@Value("${business.service.host}")
	private String authServiceHost;	
		
    public List<Movie> getMovies() throws BackendException{
    	
    	List<Movie> movies = externalRequest();
    	
	    if (movies == null) {
	    	
	        throw new BackendException("1050", "Error al obtener las peliculas",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return movies;

    }
	  
	  
	private List<Movie> externalRequest(){
	  	
		String url = authServiceHost + "/movie";
		
		List<Movie> movies = null;
		try {
			
			movies = getRequest(url);
			
		} catch (BackendException e) {

			e.printStackTrace();
		}
		  
		return movies;
	
	}
  
	private List<Movie> getRequest(String url) throws BackendException {        

        RestTemplate restTemplate = new RestTemplate();
      
        ResponseEntity<List<Movie>> responseEntity =
        		restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Movie>>() {
        		        });   
        
	    if (responseEntity == null) {
	    	
	        throw new BackendException("1050", "Error de Conexion al Business",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return responseEntity.getBody();
    }

}
