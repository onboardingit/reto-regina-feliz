package com.reto.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.backend.exception.BackendException;
import com.reto.backend.model.Character;

@Service
public class CharacterService {
	
	@Value("${business.service.host}")
	private String authServiceHost;	
		
    public List<Character> getCharacter() throws BackendException{
    	
    	List<Character> characters = externalRequest();
    	
	    if (characters == null) {
	    	
	        throw new BackendException("1050", "Error al obtener los personajes",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return characters;

    }
	  
	  
	private List<Character> externalRequest() throws BackendException{
	  	
		String url = authServiceHost + "/character";
		
		List<Character> characters = getRequest(url);
		  
		return characters;
	
	}
  
	private List<Character> getRequest(String url) throws BackendException {        

        RestTemplate restTemplate = new RestTemplate();
      
        ResponseEntity<List<Character>> responseEntity =
        		restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Character>>() {
        		        });  
        
	    if (responseEntity == null) {
	    	
	        throw new BackendException("1050", "Error de Conexion al Business",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return responseEntity.getBody();
    }

}
