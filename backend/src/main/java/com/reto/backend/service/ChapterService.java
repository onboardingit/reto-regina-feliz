package com.reto.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.backend.exception.BackendException;
import com.reto.backend.model.Chapter;

@Service
public class ChapterService {
	
	@Value("${business.service.host}")
	private String authServiceHost;	
		
    public List<Chapter> getChapters(String bookId, String sort) throws BackendException{
    	
    	List<Chapter> chapters = externalRequest(bookId, sort);
    	
	    if (chapters == null) {
	    	
	        throw new BackendException("1050", "Error al obtener los capitulos",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return chapters;
    }
	  
	  
	private List<Chapter> externalRequest(String bookId, String sort){
	  	
		String url = authServiceHost + "/chapter/" + bookId + "/order/" + sort;
		
		List<Chapter> chapters = null;
		
		try {
			
			chapters = getRequest(url);
			
		} catch (BackendException e) {

			e.printStackTrace();
		}
		  
		return chapters;
	
	}
  
	private List<Chapter> getRequest(String url) throws BackendException {        

        RestTemplate restTemplate = new RestTemplate();
      
        ResponseEntity<List<Chapter>> responseEntity =
        		restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Chapter>>() {
        		        });   
        
	    if (responseEntity == null) {
	    	
	        throw new BackendException("1050", "Error de Conexion al Business",
	            HttpStatus.INTERNAL_SERVER_ERROR);
	        
	      }
        
        return responseEntity.getBody();
    }

}
