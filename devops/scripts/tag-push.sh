#!/bin/bash
imageName=$1
build_number=$2
branch=$3

tag="build.$branch.$build_number"

echo "Tagging image $imageName with tag: $tag and pusing it"

docker tag  $imageName $imageName:$tag
docker push $imageName:$tag
docker rmi $imageName:$tag

if [ $branch == "pdn" ]; then
  echo "Pushing latest... "
  docker push $imageName:latest
  docker rmi $imageName:latest
fi

