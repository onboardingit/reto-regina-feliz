package com.reto.business.docs;

import java.math.BigDecimal;
import java.util.List;

import com.reto.business.model.Character;
import com.reto.business.model.Movie;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@Getter
@Setter
public class CharacterDocs {
	
	@JsonProperty("docs")
	@Valid
	private List<Character> docs = null;
	
	@JsonProperty("total")
	private BigDecimal total = null;
	
	@JsonProperty("limit")
	private BigDecimal limit = null;
	
	@JsonProperty("offset")
	private BigDecimal offset = null;
	
	@JsonProperty("page")
	private BigDecimal page = null;
	
	@JsonProperty("pages")
	private BigDecimal pages = null;
	
}
