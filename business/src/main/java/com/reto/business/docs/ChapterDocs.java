package com.reto.business.docs;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.reto.business.model.Chapter;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ChapterDocs {
	
    @JsonProperty("docs")
    private List<Chapter> docs = null;

    @JsonProperty("total")
    private BigDecimal total = null;

    @JsonProperty("limit")
    private BigDecimal limit = null;

    @JsonProperty("offset")
    private BigDecimal offset = null;

    @JsonProperty("page")
    private BigDecimal page = null;

    @JsonProperty("pages")
    private BigDecimal pages = null;

}
