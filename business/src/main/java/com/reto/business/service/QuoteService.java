package com.reto.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.business.docs.CharacterDocs;
import com.reto.business.docs.MoviesDocs;
import com.reto.business.docs.QuoteDocs;
import com.reto.business.model.Character;
import com.reto.business.model.Movie;
import com.reto.business.model.Quote;
import com.reto.business.model.external.QuoteExternal;

@Service
public class QuoteService {
	
	private MovieService movieService;
	
	private CharacterService characterService;
	
	private QuotesExternalService quotesExternalService;
	
	@Autowired
	public QuoteService(MovieService movieService, CharacterService characterService, QuotesExternalService quotesExternalService) {
		
		this.movieService = movieService;
		this.characterService = characterService;
		this.quotesExternalService = quotesExternalService;
		
	}

    public List<Quote> getQuotes(String characterId, String sort){
    	
    	QuoteDocs quoteDocs = externalRequest(characterId, sort);
    	
    	MoviesDocs moviesDocs = this.movieService.getMoviesDocs();
    	
    	CharacterDocs caraCharacterDocs = this.characterService.getCharactersDocs();
    	
    	QuoteDocs quoteDocsResponse = setMovieAndCharacterNamesOnQuotes(moviesDocs, caraCharacterDocs, quoteDocs);    	

        return quoteDocsResponse.getDocs();

    }
    
    public List<Quote> getQuoteById(String quoteId){
    	
    	String url = "https://the-one-api.dev/v2/quote/" + quoteId;
    	
    	QuoteDocs quoteDocs = getRequest(url);
        
        return quoteDocs.getDocs();

    }
    
    private QuoteDocs externalRequest(String characterId, String sort){    	
    	
    	String url = "https://the-one-api.dev/v2/character/" + characterId + "/quote?" + "sort=movieName:" + sort + "&limit=25";
    	
    	QuoteDocs quoteDocs = getRequest(url);
        
        return quoteDocs;

    }

	private QuoteDocs getRequest(String url) {        

        String accessToken = "2cakMULDV-ZO4So4buzB";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+ accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        QuoteDocs responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, QuoteDocs.class).getBody();          
        
        return responseEntity;
    }
	
	private QuoteDocs setMovieAndCharacterNamesOnQuotes(MoviesDocs movies, CharacterDocs characters, QuoteDocs quotes) {

		quotes.getDocs().stream()				
			.forEach(quote -> {
				
				List<QuoteExternal> quoteList = this.quotesExternalService.getQuotesExternalById(quote.getId());
				
				quoteList.forEach(quoteRequest -> {					
					
					List<Movie> movieList = this.movieService.getMovieById(quoteRequest.getMovie());
					
					List<Character> characterList = this.characterService.getCharacterById(quoteRequest.getCharacter());
					
					movieList.forEach(movie -> {
					    quote.setMovieName(movie.getName());
					});	
					
					characterList.forEach(character -> {
					    quote.setCharacterName(character.getName());
					});	
				});				

		});				
		
		return quotes;
	}
    
}
