package com.reto.business.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.business.docs.ChapterDocs;
import com.reto.business.model.Chapter;

@Service
public class ChapterService {

    public List<Chapter> getChapters(String bookId, String sort){
    	
    	ChapterDocs chapterDocs = externalRequest(bookId, sort);

        return chapterDocs.getDocs();

    }

	private ChapterDocs externalRequest(String bookId, String sort) {
		
		
        String url = "https://the-one-api.dev/v2/book/" + bookId + "/chapter?sort=chapterName:" + sort;

        String accessToken = "2cakMULDV-ZO4So4buzB";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+ accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ChapterDocs responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, ChapterDocs.class).getBody();          
        
        return responseEntity;
    }
    
}
