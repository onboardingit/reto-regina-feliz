package com.reto.business.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.business.docs.CharacterDocs;
import com.reto.business.model.Character;

@Service
public class CharacterService{

    public List<Character> getCharacters(){
    	
        CharacterDocs characterDocs = externalRequest();

        return characterDocs.getDocs();

    }
    
    public CharacterDocs getCharactersDocs(){
    	
        CharacterDocs characterDocs = externalRequest();

        return characterDocs;

    }
    
    public List<Character> getCharacterById(String characterId){
    	
    	String url = "https://the-one-api.dev/v2/character/" + characterId;
    	
    	CharacterDocs characterDocs = getRequest(url);
        
        return characterDocs.getDocs();

    }
    
    public CharacterDocs externalRequest(){
    	
    	String url = "https://the-one-api.dev/v2/character?sort=name:asc&limit=100";
    	
    	CharacterDocs characterDocs = getRequest(url);
        
        return characterDocs;

    }

	private CharacterDocs getRequest(String url) {

        String accessToken = "2cakMULDV-ZO4So4buzB";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+ accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        CharacterDocs responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, CharacterDocs.class).getBody();          
        
        return responseEntity;
    }
    
}
