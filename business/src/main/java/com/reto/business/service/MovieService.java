package com.reto.business.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.business.docs.MoviesDocs;
import com.reto.business.model.Movie;

@Service
public class MovieService {

    public List<Movie> getMovies(){
    	
        MoviesDocs moviesDocs = externalRequest();
        
        return moviesDocs.getDocs();

    }
    
    public MoviesDocs getMoviesDocs(){
    	
        MoviesDocs moviesDocs = externalRequest();
        
        return moviesDocs;

    }
    
    public List<Movie> getMovieById(String movieId){
    	
    	String url = "https://the-one-api.dev/v2/movie/" + movieId;
    	
        MoviesDocs moviesDocs = getRequest(url);
        
        return moviesDocs.getDocs();

    }
    
    public MoviesDocs externalRequest(){
    	
    	String url = "https://the-one-api.dev/v2/movie?sort=name:asc";
    	
        MoviesDocs moviesDocs = getRequest(url);
        
        return moviesDocs;

    }
    

	private MoviesDocs getRequest(String url) {        

        String accessToken = "2cakMULDV-ZO4So4buzB";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer "+accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        MoviesDocs responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, MoviesDocs.class).getBody();          
        
        return responseEntity;
    }
    
}
