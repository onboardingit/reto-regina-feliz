package com.reto.business.service;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.reto.business.docs.QuotesExternalDocs;
import com.reto.business.model.external.QuoteExternal;

@Service
public class QuotesExternalService {
	
    public List<QuoteExternal> getQuotesExternalById(String id){
    	
    	QuotesExternalDocs quotesExternalDocs = externalRequest(id);

        return quotesExternalDocs.getDocs();

    }

	private QuotesExternalDocs externalRequest(String quoteid) {

        String url = "https://the-one-api.dev/v2/quote/" + quoteid;

        String accessToken = "2cakMULDV-ZO4So4buzB";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", "Bearer " + accessToken);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        QuotesExternalDocs responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, QuotesExternalDocs.class).getBody();          
        
        return responseEntity;
    }
    
}
