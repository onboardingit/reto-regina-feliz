package com.reto.business.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.reto.business.model.Chapter;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class QuoteExternal {

	@JsonProperty("_id")
	private String id = null;

	@JsonProperty("dialog")
	private String dialog = null;
	
	@JsonProperty("movie")
	private String movie = null;
	
	@JsonProperty("character")
	private String character = null;
	
    public QuoteExternal id(String id) {
		this.id = id;
		return this;
	}

}
