package com.reto.business.repo;

import org.springframework.stereotype.Repository;

import com.reto.business.docs.MoviesDocs;

@Repository
public interface MovieInterface{
  
	MoviesDocs getRequest(String url);

}
