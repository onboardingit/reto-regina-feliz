package com.reto.business.repo;

import org.springframework.stereotype.Repository;

import com.reto.business.docs.ChapterDocs;


@Repository
public interface ChapterInterface{
  
	ChapterDocs externalRequest(String bookId, String sort);

}