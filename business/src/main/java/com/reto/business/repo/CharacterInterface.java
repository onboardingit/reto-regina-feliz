package com.reto.business.repo;

import org.springframework.stereotype.Repository;

import com.reto.business.docs.CharacterDocs;


@Repository
public interface CharacterInterface /*extends JpaRepository<Token, Integer>*/{
  
	CharacterDocs getRequest(String url);

}