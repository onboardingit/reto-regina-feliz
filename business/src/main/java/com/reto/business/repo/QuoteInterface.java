package com.reto.business.repo;

import org.springframework.stereotype.Repository;

import com.reto.business.docs.QuoteDocs;

@Repository
public interface QuoteInterface{
  
	QuoteDocs getRequest(String url);

}
