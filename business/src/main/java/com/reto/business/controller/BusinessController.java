package com.reto.business.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.business.api.BusinessApi;
import com.reto.business.model.Chapter;
import com.reto.business.model.Character;
import com.reto.business.model.Movie;
import com.reto.business.model.Quote;
import com.reto.business.service.ChapterService;
import com.reto.business.service.CharacterService;
import com.reto.business.service.MovieService;
import com.reto.business.service.QuoteService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/business")
public class BusinessController implements BusinessApi {

    @Autowired
    private CharacterService characterService;
    
    @Autowired
    private MovieService movieService;
    
    @Autowired
    private ChapterService chapterService;
    
    @Autowired
    private QuoteService quoteService;
    
    @GetMapping("/health-check")
    public ResponseEntity<String> healthCheck(){

        return ResponseEntity.ok("Hello Business Reto Regina Feliz");

    }
    
    @GetMapping("/movie")
    public ResponseEntity<List<Movie>> movie(){

        return ResponseEntity.ok(movieService.getMovies());

    }
    
    @GetMapping("/character")
    public ResponseEntity<List<Character>> character(){

        return ResponseEntity.ok(characterService.getCharacters());

    }
    
    @GetMapping("/chapter/{bookId}/order/{sort}")
    public ResponseEntity<List<Chapter>> chapter(@PathVariable("bookId") String bookId, 
    												@PathVariable("sort") String sort){

    	return ResponseEntity.ok(chapterService.getChapters(bookId, sort));

    }
    
    @GetMapping("/quote/{characterId}/order/{sort}")
    public ResponseEntity<List<Quote>> quote(@PathVariable("characterId") String characterId, 
    											@PathVariable("sort") String sort){

    	return ResponseEntity.ok(quoteService.getQuotes(characterId, sort));

    }
    
}
